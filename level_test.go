package log_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/dwagin/go-syslog"

	"gitlab.com/dwagin/go-log"
)

type LevelSuite struct {
	suite.Suite
}

func TestLevel(t *testing.T) {
	suite.Run(t, new(LevelSuite))
}

var levelTestCases = []struct { //nolint:govet // tests
	name           string
	value          log.Level
	expectedErr    error
	expectedString string
	expectedJSON   string
	expectedSyslog syslog.Priority
}{
	{
		name:           "panic",
		value:          log.PanicLevel,
		expectedString: "panic",
		expectedJSON:   `"panic"`,
		expectedSyslog: syslog.LOG_CRIT,
	},
	{
		name:           "fatal",
		value:          log.FatalLevel,
		expectedString: "fatal",
		expectedJSON:   `"fatal"`,
		expectedSyslog: syslog.LOG_CRIT,
	},
	{
		name:           "error",
		value:          log.ErrorLevel,
		expectedString: "error",
		expectedJSON:   `"error"`,
		expectedSyslog: syslog.LOG_ERR,
	},
	{
		name:           "warning",
		value:          log.WarnLevel,
		expectedString: "warning",
		expectedJSON:   `"warning"`,
		expectedSyslog: syslog.LOG_WARNING,
	},
	{
		name:           "notice",
		value:          log.NoticeLevel,
		expectedString: "notice",
		expectedJSON:   `"notice"`,
		expectedSyslog: syslog.LOG_NOTICE,
	},
	{
		name:           "info",
		value:          log.InfoLevel,
		expectedString: "info",
		expectedJSON:   `"info"`,
		expectedSyslog: syslog.LOG_INFO,
	},
	{
		name:           "debug",
		value:          log.DebugLevel,
		expectedString: "debug",
		expectedJSON:   `"debug"`,
		expectedSyslog: syslog.LOG_DEBUG,
	},
	{
		name:           "trace",
		value:          log.TraceLevel,
		expectedString: "trace",
		expectedJSON:   `"trace"`,
		expectedSyslog: syslog.LOG_DEBUG,
	},
	{
		name:           "unknown",
		value:          log.Level(99),
		expectedErr:    errors.New("unknown log level"),
		expectedString: "unknown",
		expectedJSON:   `"unknown"`,
		expectedSyslog: syslog.LOG_DEBUG,
	},
}

func (v *LevelSuite) TestString() {
	for i := range levelTestCases {
		test := levelTestCases[i]

		v.Run(test.name, func() {
			require := v.Require()
			require.Equal(test.expectedString, test.value.String())
		})
	}
}

func (v *LevelSuite) TestAppendJSON() {
	for i := range levelTestCases {
		test := levelTestCases[i]

		v.Run(test.name, func() {
			require := v.Require()
			buf := test.value.AppendJSON(make([]byte, 0, 1024))
			require.Equal([]byte(test.expectedJSON), buf)
		})
	}
}

func (v *LevelSuite) TestMarshalText() {
	for i := range levelTestCases {
		test := levelTestCases[i]

		v.Run(test.name, func() {
			require := v.Require()

			actual, err := test.value.MarshalText()
			if test.expectedErr == nil {
				require.NoError(err)
				require.Equal([]byte(test.expectedString), actual)
			} else {
				require.Error(err)
				require.Nil(actual)
			}
		})
	}
}

func (v *LevelSuite) TestUnmarshalText() {
	testCases := []struct { //nolint:govet // tests
		name        string
		value       []byte
		expected    log.Level
		expectedErr error
	}{
		{
			name:     "panic",
			value:    []byte("panic"),
			expected: log.PanicLevel,
		},
		{
			name:     "fatal",
			value:    []byte("fatal"),
			expected: log.FatalLevel,
		},
		{
			name:     "error",
			value:    []byte("error"),
			expected: log.ErrorLevel,
		},
		{
			name:     "warning",
			value:    []byte("warning"),
			expected: log.WarnLevel,
		},
		{
			name:     "notice",
			value:    []byte("notice"),
			expected: log.NoticeLevel,
		},
		{
			name:     "info",
			value:    []byte("info"),
			expected: log.InfoLevel,
		},
		{
			name:     "debug",
			value:    []byte("debug"),
			expected: log.DebugLevel,
		},
		{
			name:     "trace",
			value:    []byte("trace"),
			expected: log.TraceLevel,
		},
		{
			name:        "unknown",
			value:       []byte("unknown"),
			expected:    log.Level(99),
			expectedErr: errors.New("unknown log level"),
		},
	}

	for i := range testCases {
		test := testCases[i]

		v.Run(test.name, func() {
			require := v.Require()

			actual := log.Level(99)

			err := actual.UnmarshalText(test.value)
			if test.expectedErr == nil {
				require.NoError(err)
				require.Equal(test.expected, actual)
			} else {
				require.Error(err)
				require.ErrorContains(err, test.expectedErr.Error())
				require.Equal(test.expected, actual)
			}
		})
	}
}

func (v *LevelSuite) TestSyslogPriority() {
	for i := range levelTestCases {
		test := levelTestCases[i]

		v.Run(test.name, func() {
			require := v.Require()
			require.Equal(test.expectedSyslog, test.value.SyslogPriority())
		})
	}
}
