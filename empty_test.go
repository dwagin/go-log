package log_test

import (
	"testing"

	"gitlab.com/dwagin/go-log"
)

func TestEmpty(t *testing.T) {
	RunSuite(t, func() (log.Writer, error) {
		return log.Empty(), nil
	})
}

//nolint:dupl // tests
func BenchmarkEmpty(b *testing.B) {
	b.Run("Enabled", func(b *testing.B) {
		b.Run("Accumulated", func(b *testing.B) {
			baseLogger := log.New(log.Empty()).Logger()
			defer baseLogger.Release()

			newLogger := baseLogger.New().
				WithField("int", _tenInts[0]).
				WithField("ints", _tenInts).
				WithField("string", _tenStrings[0]).
				WithField("strings", _tenStrings).
				WithField("time", _tenTimes[0]).
				WithField("times", _tenTimes).
				WithField("user1", _oneUser).
				WithField("user2", _oneUser).
				WithField("users", _tenUsers).
				WithField("err", errExample).
				WithName("test").
				Logger()
			defer newLogger.Release()

			b.Run("Serial", func(b *testing.B) {
				b.ReportAllocs()
				b.ResetTimer()

				for range b.N {
					newLogger.Info(getMessage(0))
				}
			})
			b.Run("Concurrent", func(b *testing.B) {
				b.ReportAllocs()
				b.ResetTimer()

				b.RunParallel(func(pb *testing.PB) {
					for pb.Next() {
						newLogger.Info(getMessage(0))
					}
				})
			})
		})
		b.Run("New", func(b *testing.B) {
			baseLogger := log.New(log.Empty()).Logger()
			defer baseLogger.Release()

			b.Run("Serial", func(b *testing.B) {
				b.ReportAllocs()
				b.ResetTimer()

				for range b.N {
					baseLogger.New().
						WithField("int", _tenInts[0]).
						WithField("ints", _tenInts).
						WithField("string", _tenStrings[0]).
						WithField("strings", _tenStrings).
						WithField("time", _tenTimes[0]).
						WithField("times", _tenTimes).
						WithField("user1", _oneUser).
						WithField("user2", _oneUser).
						WithField("users", _tenUsers).
						WithField("err", errExample).
						WithName("test").
						Logger().
						Info(getMessage(0)).
						Release()
				}
			})
			b.Run("Concurrent", func(b *testing.B) {
				b.ReportAllocs()
				b.ResetTimer()

				b.RunParallel(func(pb *testing.PB) {
					for pb.Next() {
						baseLogger.New().
							WithField("int", _tenInts[0]).
							WithField("ints", _tenInts).
							WithField("string", _tenStrings[0]).
							WithField("strings", _tenStrings).
							WithField("time", _tenTimes[0]).
							WithField("times", _tenTimes).
							WithField("user1", _oneUser).
							WithField("user2", _oneUser).
							WithField("users", _tenUsers).
							WithField("err", errExample).
							WithName("test").
							Logger().
							Info(getMessage(0)).
							Release()
					}
				})
			})
		})
	})
}
