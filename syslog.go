//go:build !windows

package log

import (
	"fmt"
	"strings"
	"sync"
	"time"

	"gitlab.com/dwagin/go-bytebuffer"
	"gitlab.com/dwagin/go-json"
	syslogBase "gitlab.com/dwagin/go-syslog"
)

type syslog struct {
	writer *syslogBase.Writer
	cache  *bytebuffer.ByteBuffer
	once   sync.Once
}

var syslogPool sync.Pool

func acquireSyslog() *syslog {
	if item := syslogPool.Get(); item != nil {
		return item.(*syslog) //nolint:errcheck,forcetypeassert // redundant
	}

	return new(syslog)
}

func Syslog(tag string) (Writer, error) {
	s, err := syslogBase.New(
		syslogBase.WithPriority(syslogBase.LOG_DEBUG|syslogBase.LOG_LOCAL0),
		syslogBase.WithTag(strings.ToLower(tag)),
	)
	if err != nil {
		return nil, fmt.Errorf("syslog error: %v", err)
	}

	w := acquireSyslog()

	w.writer = s

	return w, nil
}

func (v *syslog) New() Writer {
	w := acquireSyslog()

	w.writer = v.writer

	return w
}

func (v *syslog) WriteEvent(level Level, name string, msg []byte, fields *Fields) {
	bb := bbPool.Get()

	bb.B = append(bb.B, `{"@timestamp":`...)
	bb.B = json.AppendTime(bb.B, time.Now())

	bb.B = append(bb.B, `,"log.level":`...)
	bb.B = level.AppendJSON(bb.B)

	bb.B = append(bb.B, `,"log.logger":`...)
	bb.B = json.AppendString(bb.B, name)

	bb.B = append(bb.B, `,"message":`...)
	bb.B = json.AppendString(bb.B, msg)

	v.once.Do(func() {
		if fields != nil {
			v.cache = bbPool.Get()
			v.cache.B = fields.AppendJSON(v.cache.B)
		}
	})

	if v.cache != nil {
		bb.B = append(bb.B, `,"labels":{`...)
		bb.B = append(bb.B, v.cache.B...)
		bb.B = append(bb.B, '}')
	}

	bb.B = append(bb.B, ",\"ecs.version\":\""+ecsVersion+"\"}\n"...)

	v.writer.Write(level.SyslogPriority(), bb.B) //nolint:errcheck // error ignored

	bb.Release()
}

func (v *syslog) Close() error {
	if v.writer != nil {
		s := v.writer
		v.writer = nil

		if err := s.Close(); err != nil {
			return fmt.Errorf("close syslog error: %v", err)
		}
	}

	return nil
}

func (v *syslog) Release() {
	if v == nil {
		panic("syslog: not defined")
	}

	if v.cache != nil {
		v.cache.Release()
		v.cache = nil
	}

	v.writer = nil
	v.once = sync.Once{}

	syslogPool.Put(v)
}
