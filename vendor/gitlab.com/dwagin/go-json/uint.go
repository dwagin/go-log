package json

import (
	"strconv"

	"golang.org/x/exp/constraints"
)

const smallNums = 100

func AppendUint[T constraints.Unsigned](dst []byte, src T) []byte {
	if src >= 0 && src < smallNums {
		return appendSmallNums(dst, int(src))
	}

	return strconv.AppendUint(dst, uint64(src), 10)
}

func appendSmallNums(dst []byte, src int) []byte {
	if src < 10 {
		return append(dst, byte(0x30|src))
	}

	return append(dst, byte(0x30|(src/10)), byte(0x30|(src%10)))
}
