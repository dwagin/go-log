package json

const (
	EmptyArray  = "[]"
	EmptyObject = "{}"
	Null        = "null"
)

const (
	Quote = '"'
)
