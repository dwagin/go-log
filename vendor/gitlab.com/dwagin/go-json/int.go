package json

import (
	"strconv"

	"golang.org/x/exp/constraints"
)

func AppendInt[T constraints.Signed](dst []byte, src T) []byte {
	if src >= 0 && src < smallNums {
		return appendSmallNums(dst, int(src))
	}

	return strconv.AppendInt(dst, int64(src), 10)
}
