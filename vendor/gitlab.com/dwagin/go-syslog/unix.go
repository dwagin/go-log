package syslog

import (
	"errors"
	"net"
)

// unixConnect opens a connection to the syslog daemon running on the
// local machine using a Unix domain socket.

func unixConnect() (net.Conn, error) {
	networks := []string{"unixgram", "unix"}
	paths := []string{"/var/run/log", "/var/run/syslog", "/dev/log"}

	for _, network := range networks {
		for _, path := range paths {
			if conn, err := net.Dial(network, path); err == nil {
				return conn, nil
			}
		}
	}

	return nil, errors.New("unix syslog delivery error")
}
