module gitlab.com/dwagin/go-log

go 1.23

require (
	github.com/mattn/go-colorable v0.1.14
	github.com/stretchr/testify v1.10.0
	gitlab.com/dwagin/go-bytebuffer v1.4.6
	gitlab.com/dwagin/go-json v1.0.14
	gitlab.com/dwagin/go-syslog v1.0.21
	golang.org/x/sys v0.30.0
)

require (
	github.com/davecgh/go-spew v1.1.2-0.20180830191138-d8f796af33cc // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/pmezard/go-difflib v1.0.1-0.20181226105442-5d4384ee4fb2 // indirect
	golang.org/x/exp v0.0.0-20250218142911-aa4b98e5adaa // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
