//go:build windows

package log_test

import (
	"testing"

	"gitlab.com/dwagin/go-log"
)

func TestWindows(t *testing.T) {
	RunSuite(t, func() (log.Writer, error) {
		return log.Windows("test")
	})
}
