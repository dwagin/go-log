GIT = git
GO = go
LINT = golangci-lint

all: deps vendor

lint:
	@echo ""
	@echo "Checking code..."
	@${LINT} run

deps:
	@echo ""
	@echo "Updating deps..."
	@for pkg in $$(${GO} list ./...); do \
		echo "$$pkg"; \
		${GO} get -u $$pkg; \
	done
	@${GO} mod tidy

vendor:
	@echo ""
	@echo "Vendoring..."
	@${GO} mod vendor
	@${GIT} add -A vendor

clean:
	@echo ""
	@echo "Cleaning..."
	@${GO} clean -modcache

.PHONY: all lint deps vendor clean
