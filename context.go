package log

import (
	"time"
)

type Context Logger

func New(w Writer) *Context {
	logger := acquireLogger()

	logger.name = "main"
	logger.level = InfoLevel
	logger.writer = w

	return (*Context)(logger)
}

func (v *Context) Elapsed(start time.Time) *Context {
	return v.WithField("elapsed", time.Since(start))
}

func (v *Context) WithField(key string, value any) *Context {
	if v.fields == nil {
		v.fields = AcquireFields(1)
	}

	v.fields.Append(key, value)

	return v
}

func (v *Context) WithLevel(level Level) *Context {
	if level > TraceLevel {
		level = TraceLevel
	}

	v.level = level

	return v
}

func (v *Context) WithName(name string) *Context {
	v.name = name

	return v
}

func (v *Context) Logger() *Logger {
	return (*Logger)(v)
}
