package log_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/suite"

	"gitlab.com/dwagin/go-log"
)

type LogSuite struct {
	suite.Suite
	logger    *log.Logger
	newWriter NewWriter
}

type NewWriter func() (log.Writer, error)

func RunSuite(t *testing.T, newWriter NewWriter) {
	t.Helper()

	v := &LogSuite{
		newWriter: newWriter,
	}

	suite.Run(t, v)
}

func (v *LogSuite) BeforeTest(string, string) {
	writer, err := v.newWriter()
	v.Require().NoError(err)

	v.logger = log.New(writer).Logger()
}

func (v *LogSuite) AfterTest(string, string) {
	if v.logger != nil {
		v.logger.Close()
		v.logger.Release()
		v.logger = nil
	}
}

func (v *LogSuite) TestLogger() {
	require := v.Require()
	require.Equal(log.Fields(nil), v.logger.Fields())
	require.Equal(log.InfoLevel, v.logger.Level())
	require.Equal("main", v.logger.Name())
}

func (v *LogSuite) TestLoggerNew() {
	require := v.Require()

	baseLogger := v.logger

	newLogger := baseLogger.New().Logger()
	require.NotSame(baseLogger, newLogger)

	require.Equal(baseLogger.Fields(), newLogger.Fields())
	require.Equal(baseLogger.Name(), newLogger.Name())
	require.Equal(baseLogger.Level(), newLogger.Level())
}

//nolint:funlen // tests
func (v *LogSuite) TestWithFields() {
	testCases := []struct {
		Name         string
		ParentFields log.Fields
		NewFields    log.Fields
		ResultFields log.Fields
	}{
		{
			Name:         "Nil",
			ParentFields: nil,
			NewFields:    nil,
			ResultFields: nil,
		},
		{
			Name:         "empty #1",
			ParentFields: nil,
			NewFields:    log.Fields{},
			ResultFields: nil,
		},
		{
			Name:         "empty #2",
			ParentFields: log.Fields{},
			NewFields:    nil,
			ResultFields: nil,
		},
		{
			Name:         "empty #3",
			ParentFields: nil,
			NewFields:    nil,
			ResultFields: nil,
		},
		{
			Name:         "Two #1",
			ParentFields: nil,
			NewFields: log.Fields{
				{Key: "field1", Value: "value1"},
				{Key: "field2", Value: "value2"},
			},
			ResultFields: log.Fields{
				{Key: "field1", Value: "value1"},
				{Key: "field2", Value: "value2"},
			},
		},
		{
			Name: "Two #2",
			ParentFields: log.Fields{
				{Key: "field1", Value: "value1"},
				{Key: "field2", Value: "value2"},
			},
			NewFields: nil,
			ResultFields: log.Fields{
				{Key: "field1", Value: "value1"},
				{Key: "field2", Value: "value2"},
			},
		},
		{
			Name: "Three #1",
			ParentFields: log.Fields{
				{Key: "field1", Value: "value1"},
			},
			NewFields: log.Fields{
				{Key: "field2", Value: "value2"},
				{Key: "field3", Value: "value3"},
			},
			ResultFields: log.Fields{
				{Key: "field1", Value: "value1"},
				{Key: "field2", Value: "value2"},
				{Key: "field3", Value: "value3"},
			},
		},
		{
			Name: "Three #2",
			ParentFields: log.Fields{
				{Key: "field3", Value: "value3"},
				{Key: "field2", Value: "value2"},
			},
			NewFields: log.Fields{
				{Key: "field1", Value: "value1"},
			},
			ResultFields: log.Fields{
				{Key: "field3", Value: "value3"},
				{Key: "field2", Value: "value2"},
				{Key: "field1", Value: "value1"},
			},
		},
	}

	for i := range testCases {
		test := testCases[i]

		v.Run(test.Name, func() {
			require := v.Require()

			parentContext := v.logger.New()
			for i := range test.ParentFields {
				parentContext.WithField(test.ParentFields[i].Key, test.ParentFields[i].Value)
			}

			parentLogger := parentContext.Logger()
			defer parentLogger.Release()

			newContext := parentLogger.New()
			for i := range test.NewFields {
				newContext.WithField(test.NewFields[i].Key, test.NewFields[i].Value)
			}

			newLogger := newContext.Logger()
			defer newLogger.Release()

			require.NotSame(parentLogger, newLogger)
			require.Equal(test.ResultFields, newLogger.Fields())
		})
	}
}

func (v *LogSuite) TestWithLevel() {
	require := v.Require()

	baseLogger := v.logger.New().WithLevel(log.ErrorLevel).Logger()
	defer baseLogger.Release()

	require.Equal(log.ErrorLevel, baseLogger.Level())

	newLogger := baseLogger.New().WithLevel(log.NoticeLevel).Logger()
	defer newLogger.Release()

	require.NotEqual(newLogger.Level(), baseLogger.Level())
	require.Equal(log.NoticeLevel, newLogger.Level())

	newLogger = baseLogger.New().WithLevel(log.Level(99)).Logger()
	defer newLogger.Release()

	require.NotEqual(newLogger.Level(), baseLogger.Level())
	require.Equal(log.TraceLevel, newLogger.Level())
}

func (v *LogSuite) TestWithName() {
	require := v.Require()

	baseLogger := v.logger.New().WithName("base-name").Logger()
	defer baseLogger.Release()

	newLogger := baseLogger.New().WithName("test-name").Logger()
	defer newLogger.Release()

	require.NotEqual(newLogger.Name(), baseLogger.Name())
	require.Equal("base-name", baseLogger.Name())
	require.Equal("test-name", newLogger.Name())
}

func (v *LogSuite) TestElapsed() {
	require := v.Require()

	fields := v.logger.New().Elapsed(time.Now()).Logger().Fields()

	for i := range fields {
		if fields[i].Key == "elapsed" {
			return
		}
	}

	require.Fail(`Fields() does not contain "elapsed"`)
}

func (v *LogSuite) TestLog() {
	logger := v.logger.New().WithField("field", "value").WithLevel(log.TraceLevel).Logger()
	defer logger.Release()

	logger.Log(log.TraceLevel, "test")
	logger.Fatal("test")
	logger.Error("test")
	logger.Warn("test")
	logger.Notice("test")
	logger.Info("test")
	logger.Debug("test")
	logger.Trace("test")
	logger.Print("test")
}

func (v *LogSuite) TestPanic() {
	logger := v.logger.New().WithField("field", "value").WithLevel(log.TraceLevel).Logger()
	defer logger.Release()

	defer func() {
		if err := recover(); err == nil {
			v.Fail("Panic not occurred")
		}
	}()

	logger.Panic("test")
}

func (v *LogSuite) TestLogPanic() {
	logger := v.logger.New().WithField("field", "value").WithLevel(log.TraceLevel).Logger()
	defer logger.Release()

	defer func() {
		if err := recover(); err == nil {
			v.Fail("Panic not occurred")
		}
	}()

	logger.Log(log.PanicLevel, "test")
}

func (v *LogSuite) TestLogf() {
	logger := v.logger.New().WithField("field", "value").WithLevel(log.TraceLevel).Logger()
	defer logger.Release()

	logger.Logf(log.TraceLevel, "test %s", "test")
	logger.Fatalf("test %s", "test")
	logger.Errorf("test %s", "test")
	logger.Warnf("test %s", "test")
	logger.Noticef("test %s", "test")
	logger.Infof("test %s", "test")
	logger.Debugf("test %s", "test")
	logger.Tracef("test %s", "test")
	logger.Printf("test %s", "test")
}

func (v *LogSuite) TestPanicf() {
	logger := v.logger.New().WithField("field", "value").WithLevel(log.TraceLevel).Logger()
	defer logger.Release()

	defer func() {
		if err := recover(); err == nil {
			v.Fail("Panic not occurred")
		}
	}()

	logger.Panicf("test %s", "test")
}

func (v *LogSuite) TestLogfPanic() {
	logger := v.logger.New().WithField("field", "value").WithLevel(log.TraceLevel).Logger()
	defer logger.Release()

	defer func() {
		if err := recover(); err == nil {
			v.Fail("Panic not occurred")
		}
	}()

	logger.Logf(log.PanicLevel, "test %s", "test")
}

func (v *LogSuite) TestReleasePanic() {
	logger := (*log.Logger)(nil)

	defer func() {
		if err := recover(); err == nil {
			v.Fail("Panic not occurred")
		}
	}()

	logger.Release()
}
