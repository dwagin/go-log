package log_test

import (
	"errors"
	"fmt"
	"time"

	"gitlab.com/dwagin/go-json"
)

var (
	errExample = errors.New("fail")

	_messages   = makeMessages(1000)
	_tenInts    = []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}
	_tenStrings = []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"}
	_tenTimes   = []time.Time{
		time.Unix(0, 0),
		time.Unix(1, 0),
		time.Unix(2, 0),
		time.Unix(3, 0),
		time.Unix(4, 0),
		time.Unix(5, 0),
		time.Unix(6, 0),
		time.Unix(7, 0),
		time.Unix(8, 0),
		time.Unix(9, 0),
	}
	_oneUser = &user{
		Name:      "Jane Doe",
		Email:     "jane@test.com",
		CreatedAt: time.Date(1980, 1, 1, 12, 0, 0, 0, time.UTC),
	}
	_tenUsers = users{
		_oneUser,
		_oneUser,
		_oneUser,
		_oneUser,
		_oneUser,
		_oneUser,
		_oneUser,
		_oneUser,
		_oneUser,
		_oneUser,
	}
)

func makeMessages(n int) []string {
	messages := make([]string, n)
	for i := range messages {
		messages[i] = fmt.Sprintf("Test logging, but use a somewhat realistic message length. (#%v)", i)
	}

	return messages
}

//nolint:unparam // tests
func getMessage(iter int) string {
	return _messages[iter%1000]
}

type (
	users []*user

	user struct {
		CreatedAt time.Time `json:"created_at"`
		Name      string    `json:"name"`
		Email     string    `json:"email"`
	}
)

func (v users) AppendJSON(dst []byte) []byte {
	if v == nil {
		return append(dst, json.Null...)
	}

	if len(v) == 0 {
		return append(dst, json.EmptyArray...)
	}

	dst = append(dst, '[')

	for i := range v {
		if v[i] == nil {
			dst = append(dst, json.Null...)
		} else {
			dst = append(dst, `{"name":`...)
			dst = json.AppendString(dst, v[i].Name)

			dst = append(dst, `,"email":`...)
			dst = json.AppendString(dst, v[i].Name)

			dst = append(dst, `,"created_at":`...)
			dst = json.AppendTime(dst, v[i].CreatedAt)

			dst = append(dst, '}')
		}

		dst = append(dst, ',')
	}

	dst[len(dst)-1] = ']'

	return dst
}

func (v user) AppendJSON(dst []byte) []byte {
	dst = append(dst, `{"name":`...)
	dst = json.AppendString(dst, v.Name)

	dst = append(dst, `,"email":`...)
	dst = json.AppendString(dst, v.Name)

	dst = append(dst, `,"created_at":`...)
	dst = json.AppendTime(dst, v.CreatedAt)

	dst = append(dst, '}')

	return dst
}

func makeFmtArgs() []any {
	// Need to keep this a function instead of a package-global var so that we
	// pay the cast-to-interface{} penalty on each call.
	return []any{
		_tenInts[0],
		_tenInts,
		_tenStrings[0],
		_tenStrings,
		_tenTimes[0],
		_tenTimes,
		_oneUser,
		_oneUser,
		_tenUsers,
		errExample,
	}
}
