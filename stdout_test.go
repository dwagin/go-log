//nolint:dupl // tests
package log_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/dwagin/go-log"
)

func TestStdout(t *testing.T) {
	null, err := os.OpenFile(os.DevNull, os.O_WRONLY, 0)
	require.NoError(t, err)

	stderr := os.Stderr
	stdout := os.Stdout

	defer func() {
		os.Stderr = stderr
		os.Stdout = stdout

		null.Close()
	}()

	os.Stderr = null
	os.Stdout = null

	defer func() {
		os.Stderr = stderr
		os.Stdout = stdout

		null.Close()
	}()

	RunSuite(t, func() (log.Writer, error) {
		return log.Stdout(), nil
	})
}

//nolint:dupl,funlen,maintidx // tests
func BenchmarkStdout(b *testing.B) {
	null, err := os.OpenFile(os.DevNull, os.O_WRONLY, 0)
	require.NoError(b, err)

	stderr := os.Stderr
	stdout := os.Stdout

	defer func() {
		os.Stderr = stderr
		os.Stdout = stdout

		null.Close()
	}()

	os.Stderr = null
	os.Stdout = null

	defer func() {
		os.Stderr = stderr
		os.Stdout = stdout

		null.Close()
	}()

	b.Run("Disabled", func(b *testing.B) {
		b.Run("Accumulated", func(b *testing.B) {
			baseLogger := log.New(log.Stdout()).WithLevel(log.PanicLevel).Logger()
			defer baseLogger.Release()

			newLogger := baseLogger.New().
				WithField("int", _tenInts[0]).
				WithField("ints", _tenInts).
				WithField("string", _tenStrings[0]).
				WithField("strings", _tenStrings).
				WithField("time", _tenTimes[0]).
				WithField("times", _tenTimes).
				WithField("user1", _oneUser).
				WithField("user2", _oneUser).
				WithField("users", _tenUsers).
				WithField("err", errExample).
				WithName("test").
				Logger()
			defer newLogger.Release()

			b.Run("Serial", func(b *testing.B) {
				b.ReportAllocs()
				b.ResetTimer()

				for range b.N {
					newLogger.Info(getMessage(0))
				}
			})
			b.Run("Concurrent", func(b *testing.B) {
				b.ReportAllocs()
				b.ResetTimer()

				b.RunParallel(func(pb *testing.PB) {
					for pb.Next() {
						newLogger.Info(getMessage(0))
					}
				})
			})
		})
		b.Run("New", func(b *testing.B) {
			baseLogger := log.New(log.Stdout()).WithLevel(log.PanicLevel).Logger()
			defer baseLogger.Release()

			b.Run("Serial", func(b *testing.B) {
				b.ReportAllocs()
				b.ResetTimer()

				for range b.N {
					baseLogger.New().
						WithField("int", _tenInts[0]).
						WithField("ints", _tenInts).
						WithField("string", _tenStrings[0]).
						WithField("strings", _tenStrings).
						WithField("time", _tenTimes[0]).
						WithField("times", _tenTimes).
						WithField("user1", _oneUser).
						WithField("user2", _oneUser).
						WithField("users", _tenUsers).
						WithField("err", errExample).
						WithName("test").
						Logger().
						Info(getMessage(0)).
						Release()
				}
			})
			b.Run("Concurrent", func(b *testing.B) {
				b.ReportAllocs()
				b.ResetTimer()

				b.RunParallel(func(pb *testing.PB) {
					for pb.Next() {
						baseLogger.New().
							WithField("int", _tenInts[0]).
							WithField("ints", _tenInts).
							WithField("string", _tenStrings[0]).
							WithField("strings", _tenStrings).
							WithField("time", _tenTimes[0]).
							WithField("times", _tenTimes).
							WithField("user1", _oneUser).
							WithField("user2", _oneUser).
							WithField("users", _tenUsers).
							WithField("err", errExample).
							WithName("test").
							Logger().
							Info(getMessage(0)).
							Release()
					}
				})
			})
		})
	})
	b.Run("Enabled", func(b *testing.B) {
		b.Run("Accumulated", func(b *testing.B) {
			baseLogger := log.New(log.Stdout()).WithLevel(log.InfoLevel).Logger()
			defer baseLogger.Release()

			newLogger := baseLogger.New().
				WithField("int", _tenInts[0]).
				WithField("ints", _tenInts).
				WithField("string", _tenStrings[0]).
				WithField("strings", _tenStrings).
				WithField("time", _tenTimes[0]).
				WithField("times", _tenTimes).
				WithField("user1", _oneUser).
				WithField("user2", _oneUser).
				WithField("users", _tenUsers).
				WithField("err", errExample).
				WithName("test").
				Logger()
			defer newLogger.Release()

			b.Run("Serial", func(b *testing.B) {
				b.ReportAllocs()
				b.ResetTimer()

				for range b.N {
					newLogger.Info(getMessage(0))
				}
			})
			b.Run("Concurrent", func(b *testing.B) {
				b.ReportAllocs()
				b.ResetTimer()

				b.RunParallel(func(pb *testing.PB) {
					for pb.Next() {
						newLogger.Info(getMessage(0))
					}
				})
			})
		})
		b.Run("New", func(b *testing.B) {
			baseLogger := log.New(log.Stdout()).WithLevel(log.InfoLevel).Logger()
			defer baseLogger.Release()

			b.Run("Serial", func(b *testing.B) {
				b.ReportAllocs()
				b.ResetTimer()

				for range b.N {
					baseLogger.New().
						WithField("int", _tenInts[0]).
						WithField("ints", _tenInts).
						WithField("string", _tenStrings[0]).
						WithField("strings", _tenStrings).
						WithField("time", _tenTimes[0]).
						WithField("times", _tenTimes).
						WithField("user1", _oneUser).
						WithField("user2", _oneUser).
						WithField("users", _tenUsers).
						WithField("err", errExample).
						WithName("test").
						Logger().
						Info(getMessage(0)).
						Release()
				}
			})
			b.Run("Concurrent", func(b *testing.B) {
				b.ReportAllocs()
				b.ResetTimer()

				b.RunParallel(func(pb *testing.PB) {
					for pb.Next() {
						baseLogger.New().
							WithField("int", _tenInts[0]).
							WithField("ints", _tenInts).
							WithField("string", _tenStrings[0]).
							WithField("strings", _tenStrings).
							WithField("time", _tenTimes[0]).
							WithField("times", _tenTimes).
							WithField("user1", _oneUser).
							WithField("user2", _oneUser).
							WithField("users", _tenUsers).
							WithField("err", errExample).
							WithName("test").
							Logger().
							Info(getMessage(0)).
							Release()
					}
				})
			})
		})
	})
	b.Run("Formatting", func(b *testing.B) {
		b.Run("Disabled", func(b *testing.B) {
			b.Run("Accumulated", func(b *testing.B) {
				baseLogger := log.New(log.Stdout()).WithLevel(log.PanicLevel).Logger()
				defer baseLogger.Release()

				newLogger := baseLogger.New().
					WithField("int", _tenInts[0]).
					WithField("ints", _tenInts).
					WithField("string", _tenStrings[0]).
					WithField("strings", _tenStrings).
					WithField("time", _tenTimes[0]).
					WithField("times", _tenTimes).
					WithField("user1", _oneUser).
					WithField("user2", _oneUser).
					WithField("users", _tenUsers).
					WithField("err", errExample).
					WithName("test").
					Logger()
				defer newLogger.Release()

				b.Run("Serial", func(b *testing.B) {
					b.ReportAllocs()
					b.ResetTimer()

					for range b.N {
						newLogger.Infof("%v %v %v %s %v %v %v %v %v %s", makeFmtArgs()...)
					}
				})
				b.Run("Concurrent", func(b *testing.B) {
					b.ReportAllocs()
					b.ResetTimer()

					b.RunParallel(func(pb *testing.PB) {
						for pb.Next() {
							newLogger.Infof("%v %v %v %s %v %v %v %v %v %s", makeFmtArgs()...)
						}
					})
				})
			})
			b.Run("New", func(b *testing.B) {
				baseLogger := log.New(log.Stdout()).WithLevel(log.PanicLevel).Logger()
				defer baseLogger.Release()

				b.Run("Serial", func(b *testing.B) {
					b.ReportAllocs()
					b.ResetTimer()

					for range b.N {
						baseLogger.New().
							WithField("int", _tenInts[0]).
							WithField("ints", _tenInts).
							WithField("string", _tenStrings[0]).
							WithField("strings", _tenStrings).
							WithField("time", _tenTimes[0]).
							WithField("times", _tenTimes).
							WithField("user1", _oneUser).
							WithField("user2", _oneUser).
							WithField("users", _tenUsers).
							WithField("err", errExample).
							WithName("test").
							Logger().
							Infof("%v %v %v %s %v %v %v %v %v %s", makeFmtArgs()...).
							Release()
					}
				})
				b.Run("Concurrent", func(b *testing.B) {
					b.ReportAllocs()
					b.ResetTimer()

					b.RunParallel(func(pb *testing.PB) {
						for pb.Next() {
							baseLogger.New().
								WithField("int", _tenInts[0]).
								WithField("ints", _tenInts).
								WithField("string", _tenStrings[0]).
								WithField("strings", _tenStrings).
								WithField("time", _tenTimes[0]).
								WithField("times", _tenTimes).
								WithField("user1", _oneUser).
								WithField("user2", _oneUser).
								WithField("users", _tenUsers).
								WithField("err", errExample).
								WithName("test").
								Logger().
								Infof("%v %v %v %s %v %v %v %v %v %s", makeFmtArgs()...).
								Release()
						}
					})
				})
			})
		})
		b.Run("Enabled", func(b *testing.B) {
			b.Run("Accumulated", func(b *testing.B) {
				baseLogger := log.New(log.Stdout()).WithLevel(log.InfoLevel).Logger()
				defer baseLogger.Release()

				newLogger := baseLogger.New().
					WithField("int", _tenInts[0]).
					WithField("ints", _tenInts).
					WithField("string", _tenStrings[0]).
					WithField("strings", _tenStrings).
					WithField("time", _tenTimes[0]).
					WithField("times", _tenTimes).
					WithField("user1", _oneUser).
					WithField("user2", _oneUser).
					WithField("users", _tenUsers).
					WithField("err", errExample).
					WithName("test").
					Logger()
				defer newLogger.Release()

				b.Run("Serial", func(b *testing.B) {
					b.ReportAllocs()
					b.ResetTimer()

					for range b.N {
						newLogger.Infof("%v %v %v %s %v %v %v %v %v %s", makeFmtArgs()...)
					}
				})
				b.Run("Concurrent", func(b *testing.B) {
					b.ReportAllocs()
					b.ResetTimer()

					b.RunParallel(func(pb *testing.PB) {
						for pb.Next() {
							newLogger.Infof("%v %v %v %s %v %v %v %v %v %s", makeFmtArgs()...)
						}
					})
				})
			})
			b.Run("New", func(b *testing.B) {
				baseLogger := log.New(log.Stdout()).WithLevel(log.InfoLevel).Logger()
				defer baseLogger.Release()

				b.Run("Serial", func(b *testing.B) {
					b.ReportAllocs()
					b.ResetTimer()

					for range b.N {
						baseLogger.New().
							WithField("int", _tenInts[0]).
							WithField("ints", _tenInts).
							WithField("string", _tenStrings[0]).
							WithField("strings", _tenStrings).
							WithField("time", _tenTimes[0]).
							WithField("times", _tenTimes).
							WithField("user1", _oneUser).
							WithField("user2", _oneUser).
							WithField("users", _tenUsers).
							WithField("err", errExample).
							WithName("test").
							Logger().
							Infof("%v %v %v %s %v %v %v %v %v %s", makeFmtArgs()...).
							Release()
					}
				})
				b.Run("Concurrent", func(b *testing.B) {
					b.ReportAllocs()
					b.ResetTimer()

					b.RunParallel(func(pb *testing.PB) {
						for pb.Next() {
							baseLogger.New().
								WithField("int", _tenInts[0]).
								WithField("ints", _tenInts).
								WithField("string", _tenStrings[0]).
								WithField("strings", _tenStrings).
								WithField("time", _tenTimes[0]).
								WithField("times", _tenTimes).
								WithField("user1", _oneUser).
								WithField("user2", _oneUser).
								WithField("users", _tenUsers).
								WithField("err", errExample).
								WithName("test").
								Logger().
								Infof("%v %v %v %s %v %v %v %v %v %s", makeFmtArgs()...).
								Release()
						}
					})
				})
			})
		})
	})
}
