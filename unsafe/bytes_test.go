package unsafe_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/dwagin/go-log/unsafe"
)

func TestStringToBytes(t *testing.T) {
	bytes := unsafe.StringToBytes("")
	require.Nil(t, bytes)
}
