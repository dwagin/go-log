package log

import (
	"fmt"
	"sync"

	"gitlab.com/dwagin/go-bytebuffer"

	"gitlab.com/dwagin/go-log/unsafe"
)

const ecsVersion = "1.6.0" // ecsVersion holds the version of ECS with which the formatter is compatible.

type Logger struct {
	fields *Fields
	writer Writer
	name   string
	level  Level
}

type Writer interface {
	New() Writer
	WriteEvent(level Level, name string, msg []byte, fields *Fields)
	Close() error
	Release()
}

var (
	bbPool     = bytebuffer.NewPool()
	loggerPool sync.Pool
)

func acquireLogger() *Logger {
	if item := loggerPool.Get(); item != nil {
		return item.(*Logger) //nolint:errcheck,forcetypeassert // redundant
	}

	return new(Logger)
}

func (v *Logger) New() *Context {
	logger := acquireLogger()

	if v.fields != nil {
		logger.fields = v.fields.Clone()
	}

	logger.name = v.name
	logger.level = v.level
	logger.writer = v.writer.New()

	return (*Context)(logger)
}

func (v *Logger) log(level Level, msg []byte) {
	v.writer.WriteEvent(level, v.name, msg, v.fields)
}

func (v *Logger) Log(level Level, msg string) *Logger {
	if level == PanicLevel {
		v.Panic(msg)
	}

	if v.Level() >= level {
		v.log(level, unsafe.StringToBytes(msg))
	}

	return v
}

func (v *Logger) Panic(msg string) {
	v.log(PanicLevel, unsafe.StringToBytes(msg))

	panic(msg)
}

func (v *Logger) Fatal(msg string) *Logger {
	if v.Level() >= FatalLevel {
		v.log(FatalLevel, unsafe.StringToBytes(msg))
	}

	return v
}

func (v *Logger) Error(msg string) *Logger {
	if v.Level() >= ErrorLevel {
		v.log(ErrorLevel, unsafe.StringToBytes(msg))
	}

	return v
}

func (v *Logger) Warn(msg string) *Logger {
	if v.Level() >= WarnLevel {
		v.log(WarnLevel, unsafe.StringToBytes(msg))
	}

	return v
}

func (v *Logger) Notice(msg string) *Logger {
	if v.Level() >= NoticeLevel {
		v.log(NoticeLevel, unsafe.StringToBytes(msg))
	}

	return v
}

func (v *Logger) Info(msg string) *Logger {
	if v.level >= InfoLevel {
		v.log(InfoLevel, unsafe.StringToBytes(msg))
	}

	return v
}

func (v *Logger) Debug(msg string) *Logger {
	if v.Level() >= DebugLevel {
		v.log(DebugLevel, unsafe.StringToBytes(msg))
	}

	return v
}

func (v *Logger) Trace(msg string) *Logger {
	if v.Level() >= TraceLevel {
		v.log(TraceLevel, unsafe.StringToBytes(msg))
	}

	return v
}

func (v *Logger) Print(msg string) *Logger {
	v.log(NoticeLevel, unsafe.StringToBytes(msg))
	return v
}

func (v *Logger) logf(level Level, format string, args ...any) {
	msg := bbPool.Get()
	msg.B = fmt.Appendf(msg.B, format, args...)

	v.writer.WriteEvent(level, v.name, msg.B, v.fields)

	msg.Release()
}

func (v *Logger) Logf(level Level, format string, args ...any) *Logger {
	if level == PanicLevel {
		v.Panicf(format, args...)
	}

	if v.Level() >= level {
		v.logf(level, format, args...)
	}

	return v
}

func (v *Logger) Panicf(format string, args ...any) {
	msg := bbPool.Get()
	msg.B = fmt.Appendf(msg.B, format, args...)

	v.writer.WriteEvent(PanicLevel, v.name, msg.B, v.fields)

	p := string(msg.B)

	msg.Release()

	panic(p)
}

func (v *Logger) Fatalf(format string, args ...any) *Logger {
	if v.Level() >= FatalLevel {
		v.logf(FatalLevel, format, args...)
	}

	return v
}

func (v *Logger) Errorf(format string, args ...any) *Logger {
	if v.Level() >= ErrorLevel {
		v.logf(ErrorLevel, format, args...)
	}

	return v
}

func (v *Logger) Warnf(format string, args ...any) *Logger {
	if v.Level() >= WarnLevel {
		v.logf(WarnLevel, format, args...)
	}

	return v
}

func (v *Logger) Noticef(format string, args ...any) *Logger {
	if v.Level() >= NoticeLevel {
		v.logf(NoticeLevel, format, args...)
	}

	return v
}

func (v *Logger) Infof(format string, args ...any) *Logger {
	if v.Level() >= InfoLevel {
		v.logf(InfoLevel, format, args...)
	}

	return v
}

func (v *Logger) Debugf(format string, args ...any) *Logger {
	if v.Level() >= DebugLevel {
		v.logf(DebugLevel, format, args...)
	}

	return v
}

func (v *Logger) Tracef(format string, args ...any) *Logger {
	if v.Level() >= TraceLevel {
		v.logf(TraceLevel, format, args...)
	}

	return v
}

func (v *Logger) Printf(format string, args ...any) *Logger {
	v.logf(NoticeLevel, format, args...)
	return v
}

func (v *Logger) Fields() Fields {
	if v.fields != nil {
		return *v.fields
	}

	return nil
}

func (v *Logger) Level() Level {
	return v.level
}

func (v *Logger) Name() string {
	return v.name
}

func (v *Logger) Close() error {
	return v.writer.Close() //nolint:wrapcheck // transparent wrapper
}

func (v *Logger) Release() {
	if v == nil {
		panic("Logger: not defined")
	}

	if v.fields != nil {
		v.fields.Release()
		v.fields = nil
	}

	if v.writer != nil {
		v.writer.Release()
		v.writer = nil
	}

	loggerPool.Put(v)
}
