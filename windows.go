//go:build windows

package log

import (
	"fmt"
	"sync"

	"gitlab.com/dwagin/go-bytebuffer"
	"golang.org/x/sys/windows/svc/eventlog"

	"gitlab.com/dwagin/go-log/unsafe"
)

type windows struct {
	writer *eventlog.Log
	cache  *bytebuffer.ByteBuffer
	once   sync.Once
}

var windowsPool sync.Pool

func acquireWindows() *windows {
	if item := windowsPool.Get(); item != nil {
		return item.(*windows) //nolint:errcheck,forcetypeassert // redundant
	}

	return new(windows)
}

func Windows(source string) (Writer, error) {
	el, err := eventlog.Open(source)
	if err != nil {
		return nil, fmt.Errorf("eventlog error: %v", err)
	}

	w := acquireWindows()

	w.writer = el

	return w, nil
}

func (v *windows) New() Writer {
	w := acquireWindows()

	w.writer = v.writer

	return w
}

func (v *windows) WriteEvent(level Level, name string, msg []byte, fields *Fields) {
	bb := bbPool.Get()

	switch level {
	case TraceLevel:
		bb.B = append(bb.B, "TRACE "...)
	case DebugLevel:
		bb.B = append(bb.B, "DEBUG "...)
	case InfoLevel:
		bb.B = append(bb.B, "INFO "...)
	case NoticeLevel:
		bb.B = append(bb.B, "NOTICE "...)
	case WarnLevel:
		bb.B = append(bb.B, "WARN "...)
	case ErrorLevel:
		bb.B = append(bb.B, "ERROR "...)
	case FatalLevel:
		bb.B = append(bb.B, "FATAL "...)
	case PanicLevel:
		bb.B = append(bb.B, "PANIC "...)
	}

	bb.B = append(bb.B, name...)
	bb.B = append(bb.B, ": "...)
	bb.B = append(bb.B, msg...)

	v.once.Do(func() {
		if fields != nil {
			s := *fields
			v.cache = bbPool.Get()

			for i := range s {
				v.cache.B = append(v.cache.B, ' ')
				v.cache.B = append(v.cache.B, s[i].Key...)
				v.cache.B = append(v.cache.B, '=')
				v.cache.B = fmt.Append(v.cache.B, s[i].Value)
			}
		}
	})

	if v.cache != nil {
		bb.B = append(bb.B, v.cache.B...)
	}

	//nolint:exhaustive // default usage
	switch level {
	case PanicLevel, FatalLevel, ErrorLevel:
		_ = v.writer.Error(1, unsafe.BytesToString(bb.B)) //nolint:errcheck // redundant
	case WarnLevel:
		_ = v.writer.Warning(1, unsafe.BytesToString(bb.B)) //nolint:errcheck // redundant
	default:
		_ = v.writer.Info(1, unsafe.BytesToString(bb.B)) //nolint:errcheck // redundant
	}

	bb.Release()
}

func (v *windows) Close() error {
	if v.writer != nil {
		el := v.writer
		v.writer = nil

		if err := el.Close(); err != nil {
			return fmt.Errorf("close eventlog error: %v", err)
		}
	}

	return nil
}

func (v *windows) Release() {
	if v == nil {
		panic("windows: not defined")
	}

	if v.cache != nil {
		v.cache.Release()
		v.cache = nil
	}

	v.writer = nil
	v.once = sync.Once{}

	windowsPool.Put(v)
}
